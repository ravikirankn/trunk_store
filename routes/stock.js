var index = require("./../index");
var express = require('express');
var router = express.Router();

/* GET stock page. */
router.get('/', function(req, res, next) {
  res.render('stock');
});

var convertStringToItem = function(itemString) {
  return {
    code: itemString.code,
    description: itemString.description,
    quantity: itemString.quantity,
    rate: itemString.rate,
    originalrate: itemString.originalrate,
    hotcake: itemString.hotcake
  };
}

router.post('/add', function(req, res, next) {
  var item = convertStringToItem(req.body);
  var query = {code: item.code},
    update = { expire: new Date() },
    options = { upsert: true, new: true, setDefaultsOnInsert: true };
  index.mongoConnection.collection('stock').update(query, item, options, function(err, doc){
    if (err) return res.send(500, { error: err });
    return res.send("succesfully saved");
  });
});

router.post('/delete', function(req, res, next) {
  var item = convertStringToItem(req.body);
  index.mongoConnection.collection('stock').remove(item, {}, function(err, docs){
    if (err) return res.send(500, { error: err });
    return res.send("succesfully deleted");
  });
});

router.get('/getAll', function(req, res, next) {
  index.mongoConnection.collection('stock').find({}, {}, function(err, docs){
    var callback = function(err, stock){
      if (err) return res.send(500, { error: err });
      return res.json(stock);
    };
    docs.toArray(callback);
  });
  
});

module.exports = router;