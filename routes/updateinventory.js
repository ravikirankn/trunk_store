var express = require('express');
var router = express.Router();
var index = require("./../index");

/* GET updateInventory page. */
router.get('/', function(req, res, next) {
	res.render('updateinventory');
});

router.post('/stock/quantity/increment', function(req, res, next) {

	var updateStock = req.body;

	if (updateStock) {
		for(var i=0; i< updateStock.length; i++) {
			var query = {};
			var update = {};

			if(updateStock[i].quantity){
				update = {
			  		$inc: {
			  			quantity: updateStock[i].quantity
			  		}
			  	};
			}

			if(updateStock[i].code){
			  	query.code = updateStock[i].code;
			}

			console.log(query, update);
			index.mongoConnection.collection('stock').update(query, update);
		}
	}

	return res.send("succesfully updated");
});

module.exports = router;