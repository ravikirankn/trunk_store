var router = require('express').Router();
var index = require("./../index");

var convertStringToOrder = function(orderString) {
  return {
    clientName: orderString.clientName,
    address: orderString.address,
    date: orderString.date,
    type: orderString.type,
    items: orderString.items,
    transportationCharges: parseInt(orderString.transportationCharges),
    total: parseInt(orderString.total),
    phone: orderString.phone
  };
}

router.get('/', function(req, res, next) {
  res.send({});
});

router.post('/add', function(req, res, next) {
  var order = convertStringToOrder(req.body);
  index.mongoConnection.collection('orders').insert(order);
  res.send("order created");
});

router.post("/delete", function(req, res, next) {
  var orderToDelete = convertStringToOrder(req.body);
  res.send("task deleted");  
});

module.exports = router;
