var express = require('express');
var router = express.Router();
var index = require("./../index");

/* GET sales page. */
router.get('/', function(req, res, next) {
  res.render('sales');
});

router.post('/get', function(req, res, next) {
  var query = {};

  if(req.body.name){
  	query.clientName = {$regex : ".*" + req.body.name + ".*"};
  }

  if(req.body.fromDate && req.body.toDate){
  	query.date = {
  		$gte: req.body.fromDate,
        $lte: req.body.toDate
  	};
  }

  if(req.body.type){
  	query.type = req.body.type;
  }

  index.mongoConnection.collection('orders').find(query, {}, function(err, docs){
    var callback = function(err, stock){
      if (err) return res.send(500, { error: err });
      return res.json(stock);
    };
    docs.toArray(callback);
  });
  
});

var convertStringToOrder = function(orderString) {
  return {
    clientName: orderString.clientName,
    address: orderString.address,
    date: orderString.date,
    type: orderString.type,
    items: orderString.items,
    transportationCharges: parseInt(orderString.transportationCharges),
    total: parseInt(orderString.total),
    phone: orderString.phone
  };
}

router.post('/delete', function(req, res, next) {
  var order = convertStringToOrder(req.body);
  index.mongoConnection.collection('orders').remove(order, {}, function(err, docs){
    if (err) return res.send(500, { error: err });
    return res.send("succesfully deleted");
  });
});

module.exports = router;