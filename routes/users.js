var index = require("./../index");
var express = require('express');
var router = express.Router();

router.get('/get/admin', function(req, res, next) {
	index.mongoConnection.collection('users').find({username: "admin"}, {}, function(err, docs){
		var callback = function(err, user){
			if (err) return res.send(500, { error: err });
			return res.json(user);
    	};
    	docs.toArray(callback);
    });
});

module.exports = router;