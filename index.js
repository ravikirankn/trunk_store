var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = express();
app.listen(3000);

// view engine setup
app.engine('html', require('ejs').renderFile);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var index = require('./routes/index');
app.use('/', index);
app.use('/index', index);

var orders = require('./routes/orders');
app.use('/orders', orders);

var stock = require('./routes/stock');
app.use('/stock', stock);

var sales = require('./routes/sales');
app.use('/sales', sales);

var vendors = require('./routes/vendors');
app.use('/vendors', vendors);

var users = require('./routes/users');
app.use('/users', users);

var updateInventory = require('./routes/updateinventory');
app.use('/updateinventory', updateInventory);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

// set mongo connection based on env in process.argv[2]
var envargs = process.argv.slice(2);
var isProd = envargs && envargs[0] && envargs[0].split('=') && envargs[0].split('=')[0] === 'env' && envargs[0].split('=')[1] === 'prod';

var mongoose = require('mongoose');
var mongoDB = isProd ? 'mongodb://ravikiran:P%40ssword1@ds231245.mlab.com:31245/trunk-store' : 'mongodb://localhost:27017/trunk-store';

mongoose.connect(mongoDB, {
  useMongoClient: true
});

exports.mongoConnection = mongoose.connection;

exports.mongoConnection.on('error', console.error.bind(console, 'MongoDB connection error:'));

exports.mongoConnection.once('open', function() {
  isProd ? console.log("Prod MongoDB connection successful") : console.log("Local MongoDB connection successful");
});

module.exports = app;