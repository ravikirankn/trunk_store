var salesController = function($http, $document,$timeout,$location, $filter) {
	var controller = this;

	controller.isAdminUser = false;

	var getAdminUser = function() {
		var success = function(data) {
			while(!controller.isAdminUser) {
				var password = prompt("Please enter the admin password", "");
				if(data.data[0].password === password) {
					controller.isAdminUser = true;
				} else {
					controller.isAdminUser = false;
				}
			}
		};
		var failure = function(err) {
			console.log("getting admin user failed");
			controller.isAdminUser = false;
		};
		$http.get('/users/get/admin').then(success, failure);
	};

	getAdminUser();
	
	controller.criteria = {
		name: "",
		fromDate: "",
		toDate: "",
		type: ""
	}

	controller.order = {
		clientName: "",
		address: "",
		date: "",
		type: "",
		items: [],
		transportationCharges: 0,
		total: 0
	}

	controller.orders = [];

	controller.totalItemsSold = 0;
	controller.totalSale = 0;
	controller.totalOriginalAmount = 0;
	controller.totalProfit = 0;

	var isValidSearchCriteria = function(){
		if(controller.criteria.name || (controller.criteria.fromDate && controller.criteria.toDate) || controller.criteria.type){
			return true;
		}
		return false;
	};


	controller.viewOrder = function(index){
		controller.viewModeOn = true;
		controller.order = controller.orders[index];
	}

	var imageBalaji = new Image();
	imageBalaji.src = 'images/balaji.jpeg';

	controller.print = function(){
		var pdf = new jsPDF();

		pdf.setFont("arial");
		pdf.setFontSize(11);
		pdf.text(83, 10, "*Sri Ganeshaya Namaha*");

		pdf.setFont("arial");
		pdf.setFontType("bold");
		pdf.setFontSize(16);
		pdf.text(75, 20, "BALAJI TRUNK STORE");

		pdf.setFont("arial");
		pdf.setFontType("normal");
		pdf.setFontSize(11);
		pdf.text(80, 25, "Siddiamber Bazar, Hyderabad");

		pdf.setFont("arial");
		pdf.setFontType("normal");
		pdf.setFontSize(14);
		pdf.text(60, 30, "Cell: 9985622962   Shop: 040-24617219");

		pdf.setFont("arial");
		pdf.setFontType("bold");
		pdf.setFontSize(16);
		pdf.text(75, 40, "ESTIMATE/QUOTATION");

		pdf.setFontType("bold");
		pdf.setFont("helvetica");
		pdf.setFontSize(12);
		pdf.text(20, 60, 'Name: ' + controller.order.clientName);
		pdf.text(20, 65, 'Date: ' + $filter('date')(controller.order.date, "dd/MM/yyyy"));
		pdf.text(20, 70, 'Address: ' + controller.order.address);

		
		var columns = ["S.No","Item Description", "Quantity", "Rate", "Amount"];
		var rows = [];

		for (var i=0; i < controller.order.items.length; i++) {
			var item = controller.order.items[i];
	        rows.push([i+1, item.description , item.quantity , item.rate , item.amount]);
	    }

	    rows.push(["", "", "", "Transportation Expenses", controller.order.transportationCharges]);
	    rows.push(["", "", "", "Total", controller.order.total]);

		pdf.autoTable(columns, rows, {
		    margin: {top: 80},
		    styles: {cellPadding: 0.5, fontSize: 13},
		    createdHeaderCell: function (cell, data) {
	            cell.styles.fillColor = [255, 255, 255];
	            cell.styles.textColor = [0, 0, 0];
	        }
		});

		pdf.save("Quotation-"+controller.order.clientName+".pdf");

	}

	controller.closeOrderDiv = function(){
		controller.viewModeOn = false;
		controller.order = {
			clientName: "",
			address: "",
			date: "",
			type: "",
			items: [],
			transportationCharges: 0,
			total: 0
		}
	};

	controller.deleteOrder = function(index){
		var success = function(data){
			controller.orderDeleted = true;
			controller.getOrders();
		};

		var failure = function(err){
			console.log("deletetion failed");
		};

		$http.post('/sales/delete', controller.orders[index]).then(success, failure);
	}

	controller.getOrders = function(){
		controller.totalItemsSold = 0;
		controller.totalSale = 0;
		controller.totalOriginalAmount = 0;
		controller.totalProfit = 0;

		controller.orders = [];
		controller.viewModeOn = false;
		controller.orderDeleted = false;
		if(isValidSearchCriteria()){
			controller.searchCriteriaInvalid = false;
			
			var success = function(data){
				var orders =data.data;
				for (var i=0; i < orders.length; i++) {
					var order = orders[i];
					order.date = new Date(order.date);
					order.itemsSold = 0;
					order.originalAmount = 0;
					for (var j=0; j < order.items.length; j++) {
				        order.itemsSold += parseInt(order.items[j].quantity);
				        if(isNaN(order.items[j].originalrate) || parseInt(order.items[j].originalrate) === 0){
				        	order.originalAmount += parseInt(order.items[j].rate) * parseInt(order.items[j].quantity);
				        }else{
				        	order.originalAmount += parseInt(order.items[j].originalrate) * parseInt(order.items[j].quantity);
				        }
				        order.profit = parseInt(order.total)- parseInt(order.originalAmount) - parseInt(order.transportationCharges);
				    }
				    //totals
			        controller.totalItemsSold += parseInt(order.itemsSold);
			        controller.totalOriginalAmount += parseInt(order.originalAmount);
			        controller.totalProfit += parseInt(order.profit);
			        controller.totalSale += parseInt(order.total);
					controller.orders.push(order);
				}
			};

			var failure = function(err){
				console.log("unable to get orders");
			};

			$http.post("/sales/get", controller.criteria).then(success, failure)
		}else{
			controller.searchCriteriaInvalid = true;
		}
	}
}

var app = angular.module('trunckstore-sales', ['ui.bootstrap']);

app.controller('SalesController', salesController);

app.directive('ngConfirmClick', [
	function(){
            return {
                link: function (scope, element, attr) {
                    var msg = attr.ngConfirmClick || "Are you sure?";
                    var clickAction = attr.confirmedClick;
                    element.bind('click',function (event) {
                        if ( window.confirm(msg) ) {
                            scope.$eval(clickAction)
                        }
                    });
                }
            };
    }]);