var updateInventoryController = function($http, $document,$timeout,$location, $filter) {
	var controller = this;
	controller.stock = [];
	controller.updateItem = {};

	var getStock = function(){
		controller.stock = [];
		var success = function(data){
			var stock =data.data;
			for (var i=0; i < stock.length; i++) {
				if(!stock[i].hotcake){
					stock[i].hotcake = false;
				}
				controller.stock.push(stock[i]);
			}
		};

		var failure = function(err){
			console.log("unable to get stock");
		};

		$http.get("/stock/getAll").then(success, failure)
	};

	getStock();

	function searchCode(codeKey, items){
	    for (var i=0; i < items.length; i++) {
	        if (items[i].code === codeKey) {
	            return items[i];
	        }
	    }
	}

	controller.itemChanged = function(item, index){
		if(item){
			var itemSelected = searchCode(item.code, controller.stock);
			if(itemSelected){
				controller.updateItem = itemSelected;
			}
		}	
	};

	controller.updateStock = function() {

		var success = function(data){
			controller.updateStockFlag = true;
			controller.updateStockFailedFlag = false;
		};

		var failure = function(err){
			controller.updateStockFailedFlag = true;
			controller.updateStockFlag = false;
		};

		if (searchCode(controller.updateItem.code, controller.stock) && parseInt(controller.addItemQuantity)) {
			controller.updateItem.quantity = parseInt(controller.updateItem.quantity) + parseInt(controller.addItemQuantity);
			$http.post('/stock/add', controller.updateItem).then(success, failure);
		} else {
			controller.updateStockFailedFlag = true;
			controller.updateStockFlag = false;
		}
		
		getStock();
	};
};

var app = angular.module('trunckstore', ['ui.bootstrap']);

app.controller('UpdateInventoryController', updateInventoryController);