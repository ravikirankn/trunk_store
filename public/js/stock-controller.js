var stockController = function($http, $document,$timeout,$location, $filter) {
	var controller = this;
	controller.stock = [];
	controller.isAdminUser = false;

	var getAdminUser = function() {
		var success = function(data) {
			while(!controller.isAdminUser) {
				var password = prompt("Please enter the admin password", "");
				if(data.data[0].password === password) {
					controller.isAdminUser = true;
				} else {
					controller.isAdminUser = false;
				}
			}
		};
		var failure = function(err) {
			console.log("getting admin user failed");
			controller.isAdminUser = false;
		};
		$http.get('/users/get/admin').then(success, failure);
	};

	getAdminUser();

	var getStock = function(){
		controller.stock = [];
		var success = function(data){
			var stock =data.data;
			for (var i=0; i < stock.length; i++) {
				stock[i].editMode = false;
				controller.stock.push(stock[i]);
			}
			controller.stock = $filter('orderBy')(controller.stock, 'code')
		};

		var failure = function(err){
			console.log("unable to get stock");
		};

		$http.get("/stock/getAll").then(success, failure)
	};

	getStock();

	controller.addItemInStock = function(){

		controller.stock.unshift({
			code: "",
			description: "",
			quantity: 0,
			rate: 0,
			originalrate: 0,
			editMode: true,
			hotcake: false
		});
	}

	controller.removeItemFromStock = function(index){
		var success = function(data){
			controller.itemDeletedFlag = true;
			controller.itemAddedFlag = false;
			getStock();
		};

		var failure = function(err){
			console.log("deletetion failed");
		};

		$http.post('/stock/delete', controller.stock[index]).then(success, failure);
	}

	controller.editItemInStock = function(index){
		controller.stock[index].editMode = true;
	}

	controller.updateItemInStock = function(index){
		controller.itemAddedFlag = false;
		controller.itemAdditionFailedFlag = false;

		var success = function(data){
			controller.itemAddedFlag = true;
			controller.itemDeletedFlag = false;
			controller.stock[index].editMode = false;
		};

		var failure = function(err){
			controller.itemAdditionFailedFlag = true;
			controller.itemDeletedFlag = false;
		};

		if(validItem(controller.stock[index])){
			//console.log(controller.stock[index]);
			$http.post('/stock/add', controller.stock[index]).then(success, failure);
		}else{
			failure();
		}
	}

	var validItem = function(item){
		
        if (item.code && item.description && !isNaN(item.quantity) && !isNaN(item.rate) && !isNaN(item.originalrate) && parseInt(item.quantity) >= 0 && parseInt(item.rate) >= 0 && parseInt(item.originalrate) >= 0) {
            return true;
        }
	    return false;
	}
}

var app = angular.module('trunckstore-stock', ['ui.bootstrap']);

app.controller('StockController', stockController);

app.directive('ngConfirmClick', [
	function(){
            return {
                link: function (scope, element, attr) {
                    var msg = attr.ngConfirmClick || "Are you sure?";
                    var clickAction = attr.confirmedClick;
                    element.bind('click',function (event) {
                        if ( window.confirm(msg) ) {
                            scope.$eval(clickAction)
                        }
                    });
                }
            };
    }]);