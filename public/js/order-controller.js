var orderController = function($http, $document,$timeout,$location, $filter) {
	var controller = this;

	controller.order = {
		clientName: "",
		address: "",
		date: "",
		type: "",
		phone: "",
		items: [
			{
				code: "",
				description: "",
				quantity: 0,
				rate: 0,
				amount: 0,
				originalrate: 0
			},
			{
				code: "",
				description: "",
				quantity: 0,
				rate: 0,
				amount: 0,
				originalrate: 0
			},
			{
				code: "",
				description: "",
				quantity: 0,
				rate: 0,
				amount: 0,
				originalrate: 0
			},
			{
				code: "",
				description: "",
				quantity: 0,
				rate: 0,
				amount: 0,
				originalrate: 0
			},
			{
				code: "",
				description: "",
				quantity: 0,
				rate: 0,
				amount: 0,
				originalrate: 0
			}
		],
		transportationCharges: 0,
		total: 0
	};

	controller.stock = [];

	var currentTime = new Date();
	var currentOffset = currentTime.getTimezoneOffset();
	var ISTOffset = 330;   // IST offset UTC +5:30

	controller.order.date = new Date(currentTime.getTime() + (ISTOffset + currentOffset)*60000);

	var getStock = function(){
		var success = function(data){
			var stock =data.data;
			for (var i=0; i < stock.length; i++) {
				if(!stock[i].hotcake){
					stock[i].hotcake = false;
				}
				controller.stock.push(stock[i]);
			}
		};

		var failure = function(err){
			console.log("unable to get stock");
		};

		$http.get("/stock/getAll").then(success, failure)
	};

	getStock();

	function searchCode(codeKey, items){
	    for (var i=0; i < items.length; i++) {
	        if (items[i].code === codeKey) {
	            return items[i];
	        }
	    }
	}

	controller.itemChanged = function(item, index){
		if(item){
			var itemSelected = searchCode(item.code, controller.stock);
			if(itemSelected){
				controller.order.items[index].code = itemSelected.code;
				controller.order.items[index].description = itemSelected.description;
				controller.order.items[index].rate = itemSelected.rate;
				controller.order.items[index].originalrate = itemSelected.originalrate;
			}
		}	
	}

	controller.addItemInOrder = function(){
		controller.order.items.push(
			{
				code: "",
				description: "",
				quantity: 0,
				rate: 0,
				amount: 0,
				originalrate: 0
			}
		);
	};

	controller.removeItemInOrder = function(index){
		controller.order.items.splice(index, 1);
		controller.recalculateOrder();
	};

	controller.recalculateOrder = function(){
		controller.order.total = 0;
		controller.order.items.forEach(function(item){
			item.amount = item.rate * item.quantity;
			controller.order.total += item.amount;
		});
		controller.order.total += parseInt(controller.order.transportationCharges); 
	};

	var validateItems = function(){
		for (var i=0; i < controller.order.items.length; i++) {
			var item = controller.order.items[i];
	        if (!item.code || !item.description) {
	            return false;
	        }
	    }
	    return true;
	};

	var orderValid = function(){
		removeEmptyItems();
		return controller.order && controller.order.clientName && controller.order.type
			&& controller.order.address && controller.order.date && (controller.order.phone || controller.order.type !== 'Credit')
			&& controller.order.items.length > 0 && validateItems();
	};

	controller.reload = function(){
		window.location.reload();
	};

	var updateStock = function() {
		var success = function(data) {
			console.log("quantity updated successfully");
		};

		var failure = function(err) {
			console.log("quantity not updated");
		};

		var updateStock = [];
		for (var i=0; i < controller.order.items.length; i++) {
			updateStock.push({
				code: controller.order.items[i].code,
				quantity: -controller.order.items[i].quantity
			});
		}

		console.log(updateStock);
		$http.post("/updateinventory/stock/quantity/increment", updateStock).then(success, failure);
	};

	controller.save = function(callback){
		controller.orderCreatedFlag = false;
		controller.orderCreationFailedFlag = false;
		controller.hideSaveButton = true;

		var success = function(data){
			controller.orderCreatedFlag = true;
			controller.hideSaveButton = true;
			
			updateStock();

			if(callback !== undefined){
				callback();
			}
		};

		var failure = function(err){
			controller.orderCreationFailedFlag = true;
			controller.hideSaveButton = false;
		};

		if(orderValid()){
			//console.log(controller.order);
			$http.post('/orders/add', controller.order).then(success, failure);
		}else{
			failure();
		}
	};

	controller.savePrint = function(){
		controller.save(controller.print);
	};

	var removeEmptyItems = function() {
		for (var i=0; i < controller.order.items.length; i++) {
	        if (JSON.stringify(controller.order.items[i]) === JSON.stringify({ code: "", description: "", quantity: 0, rate: 0, amount: 0, originalrate: 0 })) {
	        	controller.order.items.splice(i);
	        }
	    }
	};

	controller.print = function(){
		removeEmptyItems();
		var pdf = new jsPDF();

		pdf.setFont("arial");
		pdf.setFontSize(11);
		pdf.text(83, 10, "*Sri Ganeshaya Namaha*");

		pdf.setFont("arial");
		pdf.setFontType("bold");
		pdf.setFontSize(16);
		pdf.text(75, 20, "BALAJI TRUNK STORE");

		pdf.setFont("arial");
		pdf.setFontType("normal");
		pdf.setFontSize(11);
		pdf.text(80, 25, "Siddiamber Bazar, Hyderabad");

		pdf.setFont("arial");
		pdf.setFontType("normal");
		pdf.setFontSize(14);
		pdf.text(60, 30, "Cell: 9985622962   Shop: 040-24617219");

		pdf.setFont("arial");
		pdf.setFontType("bold");
		pdf.setFontSize(16);
		pdf.text(75, 40, "ESTIMATE/QUOTATION");

		pdf.setFontType("bold");
		pdf.setFont("helvetica");
		pdf.setFontSize(12);
		pdf.text(20, 60, 'Name: ' + controller.order.clientName);
		pdf.text(20, 65, 'Date: ' + $filter('date')(controller.order.date, "dd/MM/yyyy"));
		pdf.text(20, 70, 'Address: ' + controller.order.address);

		
		var columns = ["S.No","Item Description", "Quantity", "Rate", "Amount"];
		var rows = [];

		for (var i=0; i < controller.order.items.length; i++) {
			var item = controller.order.items[i];
	        rows.push([i+1, item.description , item.quantity , item.rate , item.amount]);
	    }

	    rows.push(["", "", "", "Transportation Expenses", controller.order.transportationCharges]);
	    rows.push(["", "", "", "Total", controller.order.total]);

		pdf.autoTable(columns, rows, {
		    margin: {top: 80},
		    styles: {cellPadding: 0.2, fontSize: 15},
		    createdHeaderCell: function (cell, data) {
	            cell.styles.fillColor = [255, 255, 255];
	            cell.styles.textColor = [0, 0, 0];
	        }
		});

		pdf.save("Quotation-"+controller.order.clientName+".pdf");

	};
};

var app = angular.module('trunckstore', ['ui.bootstrap']);

app.controller('OrderController', orderController);